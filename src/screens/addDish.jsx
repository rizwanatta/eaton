import {
  View,
  TouchableOpacity,
  StyleSheet,
  Image,
  FlatList,
} from "react-native";
import Spinner from "react-native-loading-spinner-overlay";
import React, { useState } from "react";
import { Ionicons } from "@expo/vector-icons";
import {
  Chip,
  Text,
  Icon,
  Dialog,
  Input as RneInput,
  Card,
} from "@rneui/themed";
import { Header } from "../components/header";
import { Button, Input } from "../components";
import { colors } from "../utils/theme";
import { ChooseMedia } from "../components/chooseMedia";
import { getStoredValue, makeBlobFromUri } from "../utils/help";
import { getDownloadURL, ref, uploadBytes } from "firebase/storage";
import { firestore, storage } from "../services/db";
import { doc, setDoc } from "firebase/firestore";

export default function AddDish({ navigation }) {
  const [pictureUri, setPictureUri] = useState(""); // if exists use this else use placeholder form above
  const [loading, setLoading] = useState();
  const [showMedia, setShowMedia] = useState(false);
  const [showIngredientModel, setShowIngredientModel] = useState(false);
  const [dishName, setDishName] = useState("");
  const [dishDescription, setDishDescription] = useState("");
  const [dishType, setDisType] = useState("non-veg");
  const [ingName, setIngName] = useState("");
  const [ingCount, setIngCount] = useState(0);

  const [ingData, setIngData] = useState([]);

  const onImgPressed = () => {
    // setShowMedia(!showMedia)
    if (showMedia === true) {
      setShowMedia(false);
    } else {
      setShowMedia(true);
    }
  };

  const handlePictureSource = (source) => {
    setPictureUri(source);
    onImgPressed(); // this will close the bottom sheet after adding the picture
  };

  const onAddIngredientPress = () => {
    setShowIngredientModel(!showIngredientModel);
  };

  const onDishIngAdded = () => {
    const ingredient = {
      ingName,
      ingCount,
    };

    ingData.push(ingredient);

    setIngData(ingData);

    //cleaer states
    //close the dialogue

    setIngName("");
    setIngCount(0);
    setShowIngredientModel(false);
  };

  const onDishSubmit = async () => {
    try {
      setLoading(true);

      // validation TODO

      const imgBlob = await makeBlobFromUri(pictureUri);
      const uid = await getStoredValue("uid");
      const imgName = `dish_${uid}${Math.random(2)}.png`;
      const storageref = ref(storage, `dishes/${imgName}`);
      const imageUploaded = await uploadBytes(storageref, imgBlob);
      const imgUrl = await getDownloadURL(storageref);

      const dishData = {
        dishName,
        dishDescription,
        imgUrl,
        dishType,
        ingData,
      };

      const dishDocName = `dish_${uid}_${Math.random(2)}`;
      const dishuploaded = await setDoc(
        doc(firestore, "dishes", dishDocName),
        dishData,
      );

      navigation.goBack()
      setLoading(false);
    } catch (error) {
      setLoading(false);
      alert(error.message);
    }

    //dish name decription type or ingredient data
    //dishimage b chaheya
    //

    // user k uri ko lo
    // uploadimage , get its download url
    // upload dish data with imgage url to dishes collection
  };

  return (
    <View style={{ flex: 1 }}>
      <Header
        onBackBtnPress={() => {
          navigation.goBack();
        }}
        headingText={"Add Dish"}
      />

      <View style={styles.upperCon}>
        <View style={styles.dishNamer}>
          <Input inputTitle={"Dish Name"} onChangeText={setDishName} />
          <Input
            inputTitle={"Dish description"}
            onChangeText={setDishDescription}
          />
        </View>

        <View style={styles.dishPhoto}>
          <TouchableOpacity style={styles.imgBtn} onPress={onImgPressed}>
            {pictureUri ? (
              <Image
                source={{ uri: pictureUri }}
                style={styles.img}
                resizeMode="cover"
              />
            ) : (
              <View style={styles.phIcon}>
                <Ionicons name="fast-food" size={50} color={colors.primary} />
              </View>
            )}
          </TouchableOpacity>

          <View style={styles.btnCon}>
            <Button text={"upload"} />
          </View>
        </View>
      </View>

      <Text h3>Choose Dish Type:</Text>

      <View style={styles.bottomCon}>
        <View style={styles.chipsMe}>
          <View style={styles.chipCon}>
            <Chip title="Sweets" color={colors.primary} />
          </View>
          <View style={styles.chipCon}>
            <Chip title="Vege" color={colors.primary} />
          </View>
          <View style={styles.chipCon}>
            <Chip title="Non-Vege" color={colors.black} />
          </View>
        </View>

        <View style={styles.addIngredeientHeadingCon}>
          <Text h3>Add Ingredient:</Text>
          <Icon
            name={"add"}
            type={"ionicons"}
            reverse 
            color={colors.primary}
            size={14}
            onPress={onAddIngredientPress}
          />
        </View>

        <FlatList
          data={ingData}
          renderItem={({ item }) => (
            <Card>
              <View
                style={{
                  flexDirection: "row",
                  justifyContent: "space-between",
                  alignItems: "center",
                }}
              >
                <Text h4 h4Style={{ fontWeight: "light" }}>
                  {item.ingName}
                </Text>
                <Text h4>{item.ingCount}</Text>
              </View>
            </Card>
          )}
        />

        <View style={styles.btnCon}>
          <Button text={"Submit"} onBtnPress={onDishSubmit} />
        </View>
      </View>

      <ChooseMedia
        show={showMedia}
        onClosePressed={onImgPressed}
        onPictureTaken={handlePictureSource}
      />

      {showIngredientModel && (
        <Dialog>
          <Dialog.Title title={"Add This Ingredient"} />

          <RneInput placeholder={"Ingredient Name"} onChangeText={setIngName} />
          <RneInput
            placeholder={"Ingredient count"}
            onChangeText={setIngCount}
            keyboardType={"numeric"}
          />

          <Dialog.Button title={"Add"} onPress={onDishIngAdded} />
          <Dialog.Button title={"close"} onPress={onAddIngredientPress} />
        </Dialog>
      )}
      <Spinner visible={loading} />
    </View>
  );
}

const styles = StyleSheet.create({
  upperCon: {
    flex: 0.6,
    flexDirection: "row",
    justifyContent: "space-between",
  },
  bottomCon: {
    flex: 1.4,
  },

  dishNamer: {
    flex: 1,
    paddingLeft: 10,
    justifyContent: "center",
  },
  dishPhoto: {
    flex: 1,
    justifyContent: "center",
  },
  img: {
    height: 150,
    width: 150,
    borderRadius: 75,
    resizeMode: "contain",
  },
  imgBtn: {
    alignSelf: "center",
    backgroundColor: "yellow",
    height: 150,
    width: 150,
    borderRadius: 75,
    justifyContent: "center",
    alignItems: "center",
  },
  phIcon: {
    padding: 20,
  },

  btnCon: {
    width: "80%",
    height: 40,
    alignSelf: "center",
    margin: 5,
    marginTop: 10,
  },

  chipsMe: {
    flexDirection: "row",
    width: "100%",
    justifyContent: "center",
  },

  chipCon: {
    padding: 10,
  },
  addIngredeientHeadingCon: {
    flexDirection: "row",
    alignItems: "center",
  },
});
