import { View, StyleSheet, TouchableOpacity, Image } from "react-native";
import React, { useState, useEffect } from "react";
import { getDoc, doc, setDoc } from "firebase/firestore";
import Spinner from "react-native-loading-spinner-overlay";
import { Ionicons } from "@expo/vector-icons";
import { getDownloadURL, ref, uploadBytes } from "firebase/storage";

import { Header, Button, Input } from "../components";
import { firestore, storage } from "../services/db";
import { getStoredValue, makeBlobFromUri, setStoredValue } from "../utils/help";
import { colors } from "../utils/theme";
import { ChooseMedia } from "../components/chooseMedia";

export default function Profile({ navigation }) {
  const [profileName, setProfileName] = useState("");
  const [errorName, setErrorName] = useState("");
  const [profileGender, setProfileGender] = useState("");
  const [profileEmail, setProfileEmail] = useState("");
  const [loading, setLoading] = useState(false);
  const [showMedia, setShowMedia] = useState(false);
  const [pictureUri, setPictureUri] = useState(""); // if exists use this else use placeholder form above
  // user should come on this page loading should happen and he should get
  // his fresh data from firebase firestore

  useEffect(() => {
    attemptToFetchUserDetails();
  }, []);

  const onImgPressed = () => {
    // setShowMedia(!showMedia)
    if (showMedia === true) {
      setShowMedia(false);
    } else {
      setShowMedia(true);
    }
  };

  const attemptToFetchUserDetails = async () => {
    try {
      setLoading(true);
      const uid = await getStoredValue("uid");
      const userDocRef = doc(firestore, "Users", uid);
      const userDoc = await getDoc(userDocRef);

      if (userDoc.exists()) {
        const { email, name, gender } = userDoc.data();
        setProfileName(name);
        setProfileEmail(email);
        setProfileGender(gender);

        if (userDoc.data().profileDP) {
          setPictureUri(userDoc.data().profileDP);
        }
      } else {
        alert("no data exist");
      }

      setLoading(false);
    } catch (error) {
      alert(error.message);
      setLoading(false);
    }
  };

  // when user tris to submit the form
  const onSubmit = async () => {
    // show loading
    setLoading(true);

    try {
      const blobResponse = await makeBlobFromUri(pictureUri);
      const uid = await getStoredValue("uid");



      // const imgName = "user_pic" + uid + ".png";
      // const imgName = `user_pic${uid}.png`;
      // const storageRef = ref(storage, `profile_pictures/${imgName}`);
      const storageRef = ref(storage, `profile_pictures/user_pic${uid}.png`);














      const uploadResponse = await uploadBytes(storageRef, blobResponse);
      if (uploadResponse) {
        const uploadedImageUrl = await getDownloadURL(storageRef);

        const setUserData = await setDoc(doc(firestore, "Users", uid), {
          name: profileName,
          email: profileEmail,
          gender: profileGender,
          profileDP: uploadedImageUrl,
        });

        alert("Profile data upadated");
        // check the image path wich it got saved with and
        // take that image path and send it to user UI based user document
      }
      setLoading(false);
    } catch (error) {
      alert(error.message);
      setLoading(false);
    }
    // get image path uploaded one
    // save it our firestore with user uid details
    // stop loading
  };

  const onLogutPress = () => {
    setStoredValue("isLoggedIn", "false");
    setStoredValue("uid", "");
    navigation.replace("Login");
  };

  const handlePictureSource = (source) => {
    setPictureUri(source);
    onImgPressed(); // this will close the bottom sheet after adding the picture
  };

  return (
    <>
      <View>
        <Header
          headingText={"Profile"}
          showBackBtn={false}
          rightBtnText={"Logout"}
          onRightBtnPress={onLogutPress}
        />
        <TouchableOpacity style={styles.imgBtn} onPress={onImgPressed}>
          {pictureUri ? (
            <Image
              source={{ uri: pictureUri }}
              style={styles.img}
              resizeMode="cover"
            />
          ) : (
            <View style={styles.phIcon}>
              <Ionicons name="person-outline" size={33} color={colors.white} />
            </View>
          )}
        </TouchableOpacity>
        <View style={styles.form}>
          <Input
            onChangeText={setProfileName}
            inputTitle={"name"}
            error={errorName}
            errorMsg={errorName}
            valid={!errorName}
            value={profileName}
          />
          <Input
            inputTitle={"email"}
            valid={true}
            value={profileEmail}
            autoCapitalize={"none"}
            isEditable={false}
          />
          <View style={styles.buttonCon}>
            <Button text={"Update Profile"} onBtnPress={onSubmit} />
          </View>
        </View>
        <Spinner visible={loading} />
      </View>

      <ChooseMedia
        show={showMedia}
        onClosePressed={onImgPressed}
        onPictureTaken={handlePictureSource}
      />
    </>
  );
}

const styles = StyleSheet.create({
  buttonCon: {
    height: 45,
    width: "80%",
    alignSelf: "center",
    marginVertical: 10,
  },
  form: {
    padding: 10,
  },
  alreadyAccount: {
    flexDirection: "row",
    alignItems: "center",
    alignSelf: "flex-end",
  },
  img: {
    height: 150,
    width: 150,
    borderRadius: 75,
    resizeMode: "contain",
  },
  imgBtn: {
    alignSelf: "center",
  },
  phIcon: {
    backgroundColor: colors.primary,
    padding: 20,
    borderRadius: 100,
  },
});
