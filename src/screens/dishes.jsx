import { View, FlatList, Text } from "react-native";
import React, { useEffect, useState } from "react";
import { collection, getDocs, deleteDoc, doc } from "firebase/firestore";
import { ListItem, Avatar, Button } from "@rneui/themed";
import Spinner from "react-native-loading-spinner-overlay";

import { Header } from "../components";
import { firestore } from "../services/db";
import reactotron from "reactotron-react-native";
import { colors } from "../utils/theme";

export default function Dishes({ navigation }) {
  const [dishes, setDishes] = useState([]);
  const [refresh, setRefresh] = useState(false);
  const [loading, setLoading] = useState(false);

  useEffect(() => {
    getDishes();
  }, []);

  async function getDishes() {
    setLoading(true);
    try {
      const querySnapshot = await getDocs(collection(firestore, "dishes"));

      let tempData = [];
      querySnapshot.forEach((doc) => {
        let dish = doc.data();
        let tempDish = { data: dish, dishId: doc.id };
        tempData.push(tempDish);
      });

      setDishes(tempData);
      setLoading(false);
    } catch (error) {
      reactotron.logImportant(error.message);
      setLoading(false);
    }
  }

  const onDishAddPress = () => {
    navigation.navigate("AddDish");
  };

  async function onDishDeletePress(reset, dish) {
    setLoading(true);
    try {
      await deleteDoc(doc(firestore, "dishes", dish.dishId));
      console.log(dish);
      await getDishes();
      reset(); // closes the swipt button on auto
    } catch (error) {
      setLoading(false);
    }
  }

  return (
    <View>
      <Header
        headingText={"Dishes"}
        showBackBtn={false}
        rightBtnText={"Add Dish"}
        onRightBtnPress={onDishAddPress}
      />

      <FlatList
        data={dishes}
        ListEmptyComponent={<NoDishesFound />}
        style={{ marginBottom: 200 }}
        refreshing={refresh}
        onRefresh={() => getDishes()}
        renderItem={({ item }) => (
          <ListItem.Swipeable
            bottomDivider
            leftContent={null}
            rightContent={(reset) => (
              <Button
                title="Delete"
                onPress={() => onDishDeletePress(reset, item)}
                icon={{ name: "close", color: "white", type: "ionicon" }}
                buttonStyle={{
                  minHeight: "100%",
                  backgroundColor: colors.primary,
                }}
              />
            )}
          >
            <Avatar size={"large"} rounded source={{ uri: item.data.imgUrl }} />
            <ListItem.Content>
              <ListItem.Title>{item.data.dishName}</ListItem.Title>
              <ListItem.Subtitle>{item.data.dishDescription}</ListItem.Subtitle>
            </ListItem.Content>
          </ListItem.Swipeable>
        )}
      />

      <Spinner visible={loading} />
    </View>
  );
}

function NoDishesFound() {
  return (
    <View style={{ flex: 1, justifyContent: "center", alignItems: "center" }}>
      <Text> No dishes found 😢</Text>
    </View>
  );
}
