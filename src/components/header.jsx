import { View, Text, StyleSheet, TouchableOpacity } from "react-native";
import React from "react";
import { Ionicons } from "@expo/vector-icons";
import { colors } from "../utils/theme";

function Header({
  headingText,
  showBackBtn = true,
  onBackBtnPress,
  rightBtnText,
  onRightBtnPress,
}) {
  return (
    <View style={styles.container}>
      {showBackBtn === true && (
        <TouchableOpacity onPress = {onBackBtnPress} >
          <Ionicons name={"arrow-back"} color={colors.black} size={25} />
        </TouchableOpacity>
      )}
      <View style={styles.textCon}>
        <Text style={styles.headingText}>{headingText}</Text>

        <TouchableOpacity style={styles.rightBtnCon} onPress={onRightBtnPress}>
          <Text style={styles.rightButtonText}>{rightBtnText}</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    width: "100%",
    height: 100,
    marginTop: 20,
    justifyContent: "space-evenly",
    paddingLeft: 10,
  },

  textCon: {
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
  },
  rightBtnCon: {
    marginRight: 5,
  },
  rightButtonText: {
    fontSize: 15,
    fontWeight: "500",
    color: colors.danger,
  },

  headingText: {
    fontSize: 25,
    fontWeight: "500",
  },
});

export { Header };
