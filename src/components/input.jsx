import { View, StyleSheet, TextInput, Text, Animated } from "react-native";
import React, { useEffect } from "react";
import { Ionicons } from "@expo/vector-icons";

import { colors } from "../utils/theme";

function Input({
  inputTitle,
  isSecure,
  error,
  errorMsg,
  valid,
  onChangeText,
  keyboardType,
  autoCapitalize,
  value,
  isEditable = true,
}) {
  const shakeAnimationValue = new Animated.Value(0);

  useEffect(() => {
    if (error) {
      shakeAnimation();
    }
  }, [error]);

  const shakeAnimation = () => {
    Animated.sequence([
      Animated.timing(shakeAnimationValue, {
        toValue: -10,
        duration: 100,
        useNativeDriver: true,
      }),
      Animated.timing(shakeAnimationValue, {
        toValue: 10,
        duration: 100,
        useNativeDriver: true,
      }),
      Animated.timing(shakeAnimationValue, {
        toValue: -10,
        duration: 100,
        useNativeDriver: true,
      }),
      Animated.timing(shakeAnimationValue, {
        toValue: 0,
        duration: 100,
        useNativeDriver: true,
      }),
    ]).start();
  };

  const inputStyle = {
    transform: [{ translateX: shakeAnimationValue }],
  };

  return (
    <>
      <Animated.View
        style={[
          styles.container,
          error && { borderColor: colors.danger, borderWidth: 1 },
          inputStyle,
        ]}
      >
        <Text style={[styles.title, error && { color: colors.danger }]}>
          {inputTitle.toUpperCase()}
        </Text>

        <View style={styles.inputSubCon}>
          <TextInput
            onChangeText={onChangeText}
            style={styles.input}
            secureTextEntry={isSecure}
            keyboardType={keyboardType}
            autoCapitalize={autoCapitalize}
            value={value}
            editable={isEditable}
          />
          {error && <Ionicons name={"close"} color={colors.danger} size={20} />}
          {valid && (
            <Ionicons name={"checkmark"} color={colors.success} size={20} />
          )}
        </View>
      </Animated.View>

      {error && <Text style={styles.error}> {errorMsg} </Text>}
    </>
  );
}

const styles = StyleSheet.create({
  container: {
    width: "100%",
    height: 60,
    padding: 10,
    borderRadius: 10,
    marginVertical: 10,
    justifyContent: "center",
    backgroundColor: colors.white,
  },

  inputSubCon: {
    flexDirection: "row",
    alignItems: "center",
  },

  input: {
    width: "95%",
  },

  title: {
    fontSize: 10,
    color: colors.secondary,
  },

  error: {
    fontSize: 10,
    color: colors.danger,
  },
});

export { Input };
