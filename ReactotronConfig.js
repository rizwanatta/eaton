
import Reactotron, {openInEditor, networking, trackGlobalErrors} from 'reactotron-react-native';

import { NativeModules } from 'react-native';

const hostname = NativeModules.SourceCode.scriptURL
  .split('://')[1] // Remove the scheme
  .split('/')[0] // Remove the path
  .split(':')[0]; // Remove the port

const reactotronConfig = Reactotron.configure({
  // host: '192.168.1.100',
  // here add your network IP on which the testing device/s is on
  name: 'Eaton',
  host: hostname
})
  .use(networking())
  .use(openInEditor())
  .use(trackGlobalErrors())
  .useReactNative()
  .connect();

export default reactotronConfig;
