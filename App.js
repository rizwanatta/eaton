import { AppNavigator } from "./src/navigations/appNavigator";

if(__DEV__) {
  import('./ReactotronConfig').then(() => console.log('Reactotron Configured'))
}

export default function App() {
  return <AppNavigator />;
}
